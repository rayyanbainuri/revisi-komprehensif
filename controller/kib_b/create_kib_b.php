<?php
	include "../../class/barang.php";
	$barang = new Barang();
	
	// Mengisi Attribute Dengan Hasil Dari Inputan
	$barang->register_kib_b = $_POST['register_kib_b'];
	$barang->id_jenis = $_POST['id_jenis'];
	$barang->komponen = $_POST['komponen'];
	$barang->ukuran = $_POST['ukuran'];
	$barang->satuan = $_POST['satuan'];
	$barang->tgl_perolehan = $_POST['tgl_perolehan'];
	$barang->bahan = $_POST['bahan'];
	$barang->merk = $_POST['merk'];
	$barang->type = $_POST['type'];
	$barang->tgl_bpkb = $_POST['tgl_bpkb'];
	$barang->nomor_bpkb = $_POST['nomor_bpkb'];
	$barang->no_chasis = $_POST['no_chasis'];
	$barang->no_mesin = $_POST['no_mesin'];
	$barang->no_polisi = $_POST['no_polisi'];
	$barang->asal_usul_perolehan = $_POST['asal_usul_perolehan'];
	$barang->harga = $_POST['harga'];
	$barang->keterangan = $_POST['keterangan'];
	$fileName = $_FILES['gambar']['name'];
	$barang->gambar = $fileName;;
	$barang->permasalahan_kondisi_bmd = $_POST['permasalahan_kondisi_bmd'];
	
	 
  	// Simpan di Folder Gambar
 	 move_uploaded_file($_FILES['gambar']['tmp_name'], "../../images/".$_FILES['gambar']['name']);
  
	// Menampung Hasil Dari Method Create
	$error = $barang->create_kib_b();
	
	// Pengechekan Error atau berhasil, !$error = berhasil
	if(!$error) {
		// Memanggil Tampilan Detail Dengan Mengirimkan page dan nrp
		header("Location: ../../index.php?page=data_kib_b");
	} else {
		// Membuat Session Untuk Menampilkan Pesan Error Bernama Message
		session_start();
		$_SESSION['message'] = $error;
		// Memanggil Tampilan Create Kembali
		header("Location: ../../index.php?page=form_input_kib_b");
	}
?>