<?php

	include "../../class/barang.php";
	$barang = new Barang();
	
	$barang->register_kib_a= $_GET['register_kib_a'];
	
	$error = $barang->delete_kib_a();
	
	if(!$error){
		session_start();
		$success= "<p><div class='alert text-center alert-success' role='alert'>Data Terhapus</div></p>";
		$_SESSION['message_success'] = $success;
		//memanggil tampilan detail denan mengirimkan page 
		header("location: ../../index.php?page=data_kib_a");
	}else{
		//membuat session untuk menampilkan pesan error bernama message
		session_start();
		$_SESSION['message'] = "<p><div class='alert alert-danger' role='alert'>Gagal Menghapus Data : $error </div></p>";
		//memanggil tampilan create kembali
		header("location: ../../index.php?page=data_kib_a");
	}
?>