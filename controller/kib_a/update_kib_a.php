<?php

	include "../../class/barang.php";
	$barang = new Barang();
	
	// Mengisi Attribute Dengan Hasil Dari Inputan
	$barang->register_kib_a = $_POST['register_kib_a'];
	$barang->id_jenis = $_POST['id_jenis'];
	$barang->komponen = $_POST['komponen'];
	$barang->ukuran = $_POST['ukuran'];
	$barang->satuan = $_POST['satuan'];
	$barang->tgl_perolehan = $_POST['tgl_perolehan'];
	$barang->alamat = $_POST['alamat'];
	$barang->tgl_sertifikat_tanah = $_POST['tgl_sertifikat_tanah'];
	$barang->no_sertifikat_tanah = $_POST['no_sertifikat_tanah'];
	$barang->status_tanah = $_POST['status_tanah'];
	$barang->penggunaan = $_POST['penggunaan'];
	$barang->asal_usul_perolehan = $_POST['asal_usul_perolehan'];
	$barang->harga = $_POST['harga'];
	$barang->keterangan = $_POST['keterangan'];
	$fileName = $_FILES['gambar']['name'];
	$barang->gambar = $fileName;;
	$barang->permasalahan_kondisi_bmd = $_POST['permasalahan_kondisi_bmd'];
	
	 
  	// Simpan di Folder Gambar
 	 move_uploaded_file($_FILES['gambar']['tmp_name'], "../../images/".$_FILES['gambar']['name']);
  
	// Menampung Hasil Dari Method Update
	$error = $barang->update_kib_a();
	
	// Pengechekan Error atau berhasil, !$error = berhasil
	if(!$error) {
		// Memanggil Tampilan Detail Dengan Mengirimkan Page
		header("Location: ../../index.php?page=data_kib_a");
	} else {
		// Membuat Session Untuk Menampilkan Pesan Error Bernama Message
		session_start();
		$_SESSION['message'] = $error;
		// Memanggil Tampilan Update Kembali
		header("Location: ../../index.php?page=form_ubah_kib_a");
	}
?>