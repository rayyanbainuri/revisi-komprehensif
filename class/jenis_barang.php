<?php

include_once "database.php";

class JenisBarang {
	
		// Variable Akun
	public $id_jenis;
	public $jenis_barang;
	public $kib;


//--------------------------------------------------------------------------------	
		// Menampilkan Data
//--------------------------------------------------------------------------------


		// Method untuk Menampilkan Data Akun
		public function getData($kib) {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT * from jenis_barang where kib = '{$kib}' order by jenis_barang asc";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}

		public function getDetail($id_jenis) {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT * from jenis_barang where id_jenis = '{$id_jenis}'";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data->fetch_array();
		}

	public function create(){
			$db = new Database();
		$dbConnect = $db->connect();
			// Query Untuk Menyimpan Data
		$sql = 	"INSERT INTO jenis_barang
		(
			jenis_barang,
			kib
		)
		VALUES
		(
			'{$this->jenis_barang}',
			'{$this->kib}'
	);";

			// Eksekusi Query Diatas
	$data = $dbConnect->query($sql);
			// Menampung Error Query Simpan Data
	$error = $dbConnect->error;
			// Menutup Koneksi
	$dbConnect = $db->close();
			// Mengembalikan Nilai Error
	return $error;
} 
	public function update(){
			$db = new Database();
		$dbConnect = $db->connect();
			// Query Untuk Menyimpan Data
		$sql = 	"
		UPDATE jenis_barang SET
			jenis_barang='{$this->jenis_barang}'
		WHERE
			id_jenis='{$this->id_jenis}'
	";

			// Eksekusi Query Diatas
	$data = $dbConnect->query($sql);
			// Menampung Error Query Simpan Data
	$error = $dbConnect->error;
			// Menutup Koneksi
	$dbConnect = $db->close();
			// Mengembalikan Nilai Error
	return $error;
} 

		public function delete(){
			$db = new Database();
		$dbConnect = $db->connect();
			// Query Untuk Menyimpan Data
		$sql = 	"Delete from jenis_barang
		where
			id_jenis='{$this->id_jenis}'
		";

			// Eksekusi Query Diatas
	$data = $dbConnect->query($sql);
			// Menampung Error Query Simpan Data
	$error = $dbConnect->error;
			// Menutup Koneksi
	$dbConnect = $db->close();
			// Mengembalikan Nilai Error
	return $error;
} 
	
}
?>