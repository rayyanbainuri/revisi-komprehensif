<?php if(isset($_SESSION['message'])) : ?>
		<!-- Jika Terdapat Error Maka Munculkan Pesan Pada Session Yang Telah Dibuat -->
		<p>
			<div class="alert alert-danger" role="alert"> Gagal Menyimpan Data : <?= $_SESSION['message'] ?>
			</div>
		</p>
		<!-- Mengosongkan Session Message Agar Pesan Tidak Muncul Kembali -->
		<?php unset($_SESSION['message']); ?>
<?php endif; ?>

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title"><strong>Detail Barang</strong></h4>
                <p class="card-category">KIB B (Peralatan & Mesin)</p>
            </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                      	<?php $kib_b = $barang->getDetailKib_B($_GET['register_kib_b']) ?>
							<td width="150px">
							Register</td>
							<td>:</td>
							<td><?= $kib_b['register_kib_b'] ?></td>
                      </thead>
                      <tbody>
                        <tr>
							<td>
							Jenis Barang</td>
							<td>:</td>
							<td><?php  $jn=$jenis_barang->getDetail($kib_b['id_jenis']);
                                      echo $jn['jenis_barang']; ?></td>
                        </tr>
                        <tr>
							<td>
							Komponen</td>
							<td>:</td>
							<td><?= $kib_b['komponen'] ?></td>
                        </tr>
                        <tr>
							<td>
							Ukuran</td>
							<td>:</td>
							<td><?= $kib_b['ukuran'] ?></td>
                        </tr>
						<tr>
							<td>
							Satuan</td>
							<td>:</td>
							<td><?= $kib_b['satuan'] ?></td>
                        </tr>
						<tr>
							<td>
							Tanggal Perolehan</td>
							<td>:</td>
							<td><?= date($kib_b['tgl_perolehan']) ?></td>
                        </tr>
						<tr>
							<td>
							Bahan</td>
							<td>:</td>
							<td><?= $kib_b['bahan'] ?></td>
                        </tr>
                        <tr>
							<td>
							Merk</td>
							<td>:</td>
							<td><?= $kib_b['merk'] ?></td>
                        </tr>
                        <tr>
							<td>
							Type</td>
							<td>:</td>
							<td><?= $kib_b['type'] ?></td>
                        </tr>
                        <?php if($kib_b['id_jenis']==9||$kib_b['id_jenis']==57||$kib_b['id_jenis']==52){?>
                        <tr>
							<td>
							Tanggal BPKB</td>
							<td>:</td>
							<td><?= date($kib_b['tgl_bpkb']) ?></a></td>
                        </tr>
						<tr>
							<td>
							Nomor BPKB</td>
							<td>:</td>
							<td><?= $kib_b['nomor_bpkb'] ?></td>
                        </tr>
						<tr>
							<td>
							No. Chasis / No. Rangka Mesin</td>
							<td>:</td>
							<td><?= $kib_b['no_chasis'] ?></td>
                        </tr>
						<tr>
							<td>
							No. Mesin / No. Pabrik</td>
							<td>:</td>
							<td><?= $kib_b['no_mesin'] ?></td>
                        </tr>
						<tr>
							<td>
							No. Polisi</td>
							<td>:</td>
							<td><?= $kib_b['no_polisi'] ?></td>
                        </tr>
                    <?php }?>
						<tr>
							<td>
							Asal-usul Perolehan / Anggaran</td>
							<td>:</td>
							<td><?= $kib_b['asal_usul_perolehan'] ?></td>
                        </tr>
						<tr>
							<td>
							Harga (Rp.)</td>
							<td>:</td>
							<td>Rp. <?= number_format($kib_b['harga']); ?></td>
                        </tr>
						<tr>
							<td>
							Keterangan</td>
							<td>:</td>
							<td><?= $kib_b['keterangan'] ?></td>
                        </tr>
						<tr>
							<td>
							Kondisi BMD</td>
							<td>:</td>
							<td><a class="btn btn-<?php if ($kib_b['permasalahan_kondisi_bmd']=='Baik'){echo "success";} else if ($kib_b['permasalahan_kondisi_bmd']=='Rusak'){echo "danger";} if ($kib_b['permasalahan_kondisi_bmd']=='Rusak Berat'){echo "warning";}?>" style="width:130px"><?= $kib_b['permasalahan_kondisi_bmd'] ?></a></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
			<div class="col-md-4">
           <div class="card">
             <div class="card-header card-header-primary">
                <h4 class="card-title">Gambar</h4>
              	<p class="card-category">KIB B (Peralatan & Mesin)</p>
             </div>
              <div class="card-body">
               <div class="row">
                <div class="col-sm-4 col-lg-5">
				<img style="border-radius:1%; max-width: 293px; max-height: 250px; min-height: 250px; min-width: 293px" width='293' height='250' src='images/<?= $kib_b['gambar']; ?>'/>
              <?php ?>
            <br>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>