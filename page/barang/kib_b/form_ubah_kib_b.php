<?php if(isset($_SESSION['message'])) : ?>
		<!-- Jika Terdapat Error maka akan memunculkan Pesan pada Session yang telah dibuat -->
		<p>
			<div class="alert alert-danger" role="alert"> Gagal Merubah Data : <?= $_SESSION['message'] ?>
			</div>
		</p>
		<!-- Mengosongkan Session Message agar Pesan tidak muncul kembali -->
		<?php unset($_SESSION['message']); ?>
<?php endif; ?>

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Form Ubah Data</h4>
                <p class="card-category">KIB B (Peralatan & Mesin)</p>
            </div>
                <div class="card-body">
                  <div class="table-responsive">
                  	<form action="controller/kib_b/update_kib_b.php" method="post" enctype="multipart/form-data">
                    <table class="table">
                      <thead class=" text-primary">
                      	<input type="hidden" name="register_kib_b" value="<?= $_GET['register_kib_b'] ?>"/>
                      	<?php $kib_b = $barang->getDetailKib_B($_GET['register_kib_b']); ?>
							<td width="150px">
							Register</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="register_kib_b" value="<?= $kib_b['register_kib_b'] ?>" disabled></td>
                      </thead>
                      <tbody>
                        <tr>
							<td>
							Jenis Barang</td>
							<td>:</td>
							<td><select  class="form-control" name="id_jenis" disabled="" >
									<?php foreach ($jenis_barang->getData('b') as $val ): ?>
										<option value="<?php echo $val['id_jenis'] ?>"><?php echo $val['jenis_barang'] ?></option>
									<?php endforeach ?>
								</select></td>
                        </tr>
                        <tr>
							<td>
							Komponen</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="komponen" value="<?= $kib_b['komponen'] ?>" required></td>
                        </tr>
                        <tr>
							<td>
							Ukuran</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="ukuran" value="<?= $kib_b['ukuran'] ?>" ></td>
                        </tr>
						<tr>
							<td>
							Satuan</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="satuan" value="<?= $kib_b['satuan'] ?>" ></td>
                        </tr>
						<tr>
							<td>
							Tanggal Perolehan</td>
							<td>:</td>
							<td><input type="date" class="form-control" name="tgl_perolehan" value="<?= $kib_b['tgl_perolehan'] ?>" required></td>
                        </tr>
						<tr>
							<td>
							Bahan</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="bahan" value="<?= $kib_b['bahan'] ?>"></td>
                        </tr>
                        <tr>
							<td>
							Merk</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="merk" value="<?= $kib_b['merk'] ?>"></td>
                        </tr>
                        <tr>
							<td>
							Type</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="type" value="<?= $kib_b['type'] ?>"></td>
                        </tr>
                        <?php if($kib_b['id_jenis']==9||$kib_b['id_jenis']==57||$kib_b['id_jenis']==52){?>
                        <tr >
							<td>
							Tanggal BPKB</td>
							<td>:</td>
							<td><input type="date" class="form-control" name="tgl_bpkb" value="<?= $kib_b['tgl_bpkb'] ?>"></td>
                        </tr>
						<tr>
							<td>
							Nomor BPKB</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="nomor_bpkb" value="<?= $kib_b['nomor_bpkb'] ?>"></td>
                        </tr>
						<tr>
							<td>
							No. Chasis / No. Rangka Mesin</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="no_chasis" value="<?= $kib_b['no_chasis'] ?>"></td>
                        </tr>
						<tr>
							<td>
							No. Mesin / No. Pabrik</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="no_mesin" value="<?= $kib_b['no_mesin'] ?>"></td>
                        </tr>
						<tr>
							<td>
							No. Polisi</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="no_polisi" value="<?= $kib_b['no_polisi'] ?>"></td>
                        </tr>
                    <?php }?>
						<tr>
							<td>
							Asal-usul Perolehan / Anggaran</td>
							<td>:</td>
							<td>
								<select  class="form-control" name="asal_usul_perolehan" required>
								<option value="Hibah" <?php if ($kib_b['asal_usul_perolehan']=="Hibah") {
									echo "selected";
								}?> >Hibah</option>
								<option value="Membangun Sendiri" <?php if ($kib_b['asal_usul_perolehan']=="Membangun Sendiri") {
									echo "selected";
								}?> >Membangun Sendiri</option>
								</select>
                          </td>
                        </tr>
						<tr>
							<td>
							Harga (Rp.)</td>
							<td>:</td>
							<td>Rp. <input type="number" class="form-control" name="harga" value="<?= $kib_b['harga'] ?>"></a>
                          </td>
                        </tr>
						<tr>
							<td>
							Keterangan</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="keterangan" value="<?= $kib_b['keterangan'] ?>"></a>
                          </td>
                        </tr>
						<tr>
							<td>
							Kondisi BMD</td>
							<td>:</td>
							<td><select  class="form-control" name="permasalahan_kondisi_bmd" required>
								<option value="Baik" <?php if ($kib_b['permasalahan_kondisi_bmd']=="Baik") {
									echo "selected";
								}?> >Baik</option>
								<option value="Rusak" <?php if ($kib_b['permasalahan_kondisi_bmd']=="Rusak") {
									echo "selected";
								}?> >Rusak</option>
								<option value="Rusak Berat" <?php if ($kib_b['permasalahan_kondisi_bmd']=="Rusak Berat") {
									echo "selected";
								}?> >Rusak Berat</option>
								</select></td>
                        </tr>
                      </tbody>
                    </table>
					<br>
					<center>
						<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp; Simpan Data</button>
					</center>
					<br>
                  </div>
                </div>
              </div>
            </div>
			<div class="col-md-4">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Gambar</h4>
                  <p class="card-category">KIB B (Peralatan & Mesin)</p>
                </div>
                <div class="card-body">
                  <div class="row">
				   <div class="col-sm-6 col-lg-5">
					<img style="border-radius:1% max-width: 293px; max-height: 250px; min-height: 250px; min-width: 293px" width='293' height='250' src='images/<?= $kib_b['gambar']; ?>' id="output_image" />
					<input type="file" name="gambar" accept="images/<?= $kib_a['gambar']; ?>" onchange="preview_image(event)" class="form-control" required>
                 <br>
                  </div>
                  </form>
                </div>
              </div>
            </div>
           </div>
         </div>
        