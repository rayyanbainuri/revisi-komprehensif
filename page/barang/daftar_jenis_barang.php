          <div class="row">
			<div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="fa fa-building"></i>
                  </div>
                  <h1 class="card-title"><strong>KIB A</strong></h1>
				  <p class="card-category">(Tanah)</p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <a href="index.php?page=data_kib_a" class="success-link"><strong>Kelola Data</strong></a>
                  </div>
                </div>
              </div>
            </div>
			
			<div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon">
                    <i class="fa fa-desktop"></i>
                  </div>
                  <h1 class="card-title"><strong>KIB B</strong></h1>
				  <p class="card-category">(Peralatan & Mesin)</p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <a href="index.php?page=data_kib_b" class="info-link"><strong>Kelola Data</strong></a>
                  </div>
                </div>
              </div>
            </div>
			
           </div>
            