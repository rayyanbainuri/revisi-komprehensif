        <?php 
        $bulan=array(
        "1"=>"Januari",
        "2"=>"Februari",
        "3"=>"Maret",
        "4"=>"April",
        "5"=>"Mei",
        "6"=>"Juni",
        "7"=>"Juli",
        "8"=>"Agustus",
        "9"=>"September",
        "10"=>"Oktober",
        "11"=>"November",
        "12"=>"Desember");
         $bulan2=array(
        "01"=>"Januari",
        "02"=>"Februari",
        "03"=>"Maret",
        "04"=>"April",
        "05"=>"Mei",
        "06"=>"Juni",
        "07"=>"Juli",
        "08"=>"Agustus",
        "09"=>"September",
        "10"=>"Oktober",
        "11"=>"November",
        "12"=>"Desember");
        if(isset($_POST['bulan'])){

        $bln= date('m',strtotime($_POST['bulan']));
        $tahun= date('Y',strtotime($_POST['bulan']));
        $kib= $_POST['kib'];
          
        }
        ?> 
          <div class="row">
           
           <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                  <i class="fa fa-building"></i>
                </div>
                <p class="card-category"><strong>KIB A</strong></p>
                <p class="card-category">(Tanah)</p>
                <p class="card-category">Jumlah Data</p>
                <h3 class="card-title"><?php foreach ($barang->getData_Jumlah_Kib_A() as $jumlah_data_kib_a) {
                  echo $jumlah_data_kib_a['jumlah']; 
                }
                ?>
              </h3>
            </div>
            <div class="card-footer">
              <div class="stats">
                <?php if ($_SESSION['hak_akses'] == 'pengelola') { ?>
                  <a href="index.php?page=data_kib_a" class="success-link"><strong>Kelola Data</strong></a>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
              <div class="card-icon">
                <i class="fa fa-desktop"></i>
              </div>
              <p class="card-category"><strong>KIB B</strong></p>
              <p class="card-category">(Peralatan & Mesin)</p>
              <p class="card-category">Jumlah Data</p>
              <h3 class="card-title"><?php foreach ($barang->getData_Jumlah_Kib_B() as $jumlah_data_kib_b) {
                echo $jumlah_data_kib_b['jumlah']; 
              }
              ?>
              
            </h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <?php if ($_SESSION['hak_akses'] == 'pengelola') { ?>
                <a href="index.php?page=data_kib_b" class="info-link"><strong>Kelola Data</strong></a>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header card-header-warning card-header-icon">
            <div class="card-icon">
              <i class="material-icons">storage</i>
            </div>
            <p class="card-category"><strong>Jumlah</strong></p>
            <p class="card-category">Semua Data</p>
            <h3 class="card-title"><?php $jumlahtotal = $jumlah_data_kib_a['jumlah']+$jumlah_data_kib_b['jumlah'];
            echo $jumlahtotal;

            ?>
          </h3>
          <br>
        </div>
        <div class="card-footer">
          <div class="stats">
            
          </div>
        </div>
      </div>
    </div>
            
    <div class="row">
      <div class="col-md-12 border">
        <label class="bold"><h3>Data Kib A</h3><hr></label>
        
        <div id="chart1" style="min-width: 500px; height: 300px; max-width: 1000px; margin: 0 auto"></div>
        <div class="row">
        </div>
      </div>
      <div class="col-md-12 border">
        <label class="bold"><h3>Data Kib B</h3><hr></label>
        
        <div id="chart2" style="min-width: 500px; height: 300px; max-width: 1000px; margin: 0 auto"></div>
        <div class="row">
        
        </div>
      </div>
    </div>
  </div>
  <?php include "page/chart.php"; ?>
  <script type="text/javascript">
    Highcharts.chart('container', {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'çolumn'
      },
      title: {
        text: 'Persentase Fisik Barang KIB A Yang Tersedia'
      },
      xAxis: {
          type: 'category'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
            style: {
              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
            }
          }
        }
      },
      series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [
        <?php foreach ($barang->getDataChunz1() as $key): ?>
          <?php echo "{name : '".$key['jenis_barang']."'," ?>
          <?php echo "y : ".$key['jumlah']."}," ?>
        <?php endforeach ?>  
        ]
      }]
    });




    Highcharts.chart('container2', {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'çolumn'
      },
      title: {
        text: 'Persentase Fisik Barang KIB B Yang Tersedia'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
            style: {
              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
            }
          }
        }
      },
      series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [
        <?php foreach ($barang->getDataChunz2() as $key): ?>
          <?php echo "{name : '".$key['jenis_barang']."'," ?>
          <?php echo "y : ".$key['jumlah']."}," ?>
        <?php endforeach ?>  
        ]
      }]
    });



  </script>
