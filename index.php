<?php
session_start(); 
	array_key_exists('page',$_GET) ? $page = $_GET['page'] : $page = 'home';

  if (isset($_SESSION['id_akun'])) {
  } else {
    header("location: page/login.php");
  }

  if ($page=="Logout") {
    session_destroy();
    header("location: page/login.php");
  }

  include "class/barang.php";
  $barang = new Barang();
    include "class/jenis_barang.php";
  $jenis_barang = new JenisBarang();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="assets/img/icon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>
		PAPERALIKDA Kecamatan Ciparay
	</title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="assets/demo/demo.css" rel="stylesheet" />
  <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />

    <!--   Core JS Files   -->
  <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="https://unpkg.com/default-passive-events"></script>
  <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
 
  <script src="assets/js/highcharts.js"></script>
   <script src="assets/js/exporting.js"></script>
  <!-- Place this tag in your head or just before your close body tag. -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!--  Google Maps Plugin    -->
  <!-- Chartist JS -->
  <script src="assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/material-dashboard.js?v=2.1.0"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="assets/demo/demo.js"></script>
  <script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
</head>

<body class="dark-edition">
  <div class="wrapper ">
  <div class="sidebar" data-color="purple" data-background-color="black" data-image="images/KantorKecamatan.png">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="index.php" class="simple-text logo-normal"><img width="130px" height="120px" src="assets/img/icon.png"><br><br>
          <strong>PAPERALIKDA</strong><br>
          <strong>Kecamatan Ciparay</strong>
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item <?php if($page=="home"){echo"active";}?>  ">
            <a class="nav-link" href="index.php">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
        <?php if ($_SESSION['hak_akses'] == 'pengelola') { ?>
         <li class="nav-item <?php if($page=="daftar_jenis_barang"){echo"active";}?>">
            <a class="nav-link" href="index.php?page=daftar_jenis_barang">
              <i class="material-icons">archive</i>
              <p>Data Barang</p>
            </a>
          </li>
          <li class="nav-item <?php if($page=="daftar_inven"){echo"active";}?>">
            <a class="nav-link" href="index.php?page=daftar_inven">
              <i class="fa fa-balance-scale"></i>
              <p>Data Inventaris</p>
            </a>
          </li>
        <?php } ?>
        <?php if ($_SESSION['hak_akses'] == 'kasubag') { ?>
          <li class="nav-item <?php if($page=="daftar_inven"){echo"active";}?>">
            <a class="nav-link" href="index.php?page=daftar_inven">
              <i class="fa fa-balance-scale"></i>
              <p>Data Inventaris</p>
            </a>
          </li>  
        <?php } ?>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="javascript:void(0)">
        			<h4><?php 
        			if ($page=="home") {echo "Dashboard";} 
        			elseif ($page=="daftar_jenis_barang") {echo "Data Barang";} 
        			elseif ($page=="daftar_inven") {echo "Data Inventaris";}
        			elseif ($page=="laporan") {echo "Laporan";} 
        			elseif ($page=="data_kib_a") {echo "Daftar Data KIB A";} 
        			elseif ($page=="data_kib_b") {echo "Daftar Data KIB B";} 
        			elseif ($page=="form_input_kib_a") {echo "Input Data KIB A";} 
        			elseif ($page=="form_input_kib_b") {echo "Input Data KIB B";} 
        			elseif ($page=="detail_kib_a") {echo "Detail  Data KIB A";} 
        			elseif ($page=="detail_kib_b") {echo "Detail Data KIB B";} 
        			elseif ($page=="form_ubah_kib_a") {echo "Ubah Data KIB A";} 
        			elseif ($page=="form_ubah_kib_b") {echo "Ubah Data KIB B";}
        			?></h4></a>
          </div>
          <div class="collapse navbar-collapse justify-content-end">
            <li class="nav-item dropdown">
                <a class="nav-link" href="javscript:void(0)" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="index.php?page=Logout">Logout</a>
                </div>
              </li>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="javascript:void(0)">
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
			<?php if($page=="home"){
				include "page/home.php";
				}
				elseif($page=="daftar_jenis_barang")
				{
				include "page/barang/daftar_jenis_barang.php";
				}
				elseif($page=="daftar_inven")
				{
				include "page/data_inven/daftar_inven.php";
				}
				elseif($page=="laporan")
				{
				include "page/laporan/data_laporan.php";
				}
				elseif($page=="detail_barang")
				{
				include "page/barang/detail_barang.php";
				}
				elseif($page=="data_kib_a")
				{
				include "page/barang/kib_a/data_kib_a.php";
				}
				elseif($page=="data_kib_b")
				{
				include "page/barang/kib_b/data_kib_b.php";
				}
				elseif($page=="form_input_kib_a")
				{
				include "page/barang/kib_a/form_input_kib_a.php";
				}
				elseif($page=="form_input_kib_b")
				{
				include "page/barang/kib_b/form_input_kib_b.php";
				}
				elseif($page=="detail_kib_a")
				{
				include "page/barang/kib_a/detail_kib_a.php";
				}
				elseif($page=="detail_kib_b")
				{
				include "page/barang/kib_b/detail_kib_b.php";
        }
        elseif($page=="form_ubah_kib_a")
        {
        include "page/barang/kib_a/form_ubah_kib_a.php";
        }
        elseif($page=="form_ubah_kib_b")
        {
        include "page/barang/kib_b/form_ubah_kib_b.php";
        }
				elseif($page=="form_jenis_barang")
        {
        	include "page/barang/form_jenis_barang.php";
        }
				?>
		 </div>
      </div>
      <footer class="footer">
        <div class="container-fluid">
          <nav class="float-left">
            <ul>
              <li>
                <a href=""><img width="20px" height="20px" src="assets/img/icon.png">
                  Pemerintahan Kecamatan Ciparay
                </a>
              </li>
              <li>
                <a href="">
                  
                </a>
              </li>
              <li>
                <a href="">
                  
                </a>
              </li>
              <li>
                <a href="">
                  
                </a>
              </li>
            </ul>
          </nav>
          <div class="copyright float-right" id="date">
            
          </div>
        </div>
      </footer>

    </div>
  </div>
<script>
        const f = new Date().getFullYear();
        let date = document.getElementById('date');
        date.innerHTML = 'Rayyan Bainuri ' + '&copy; ' + f + date.innerHTML;
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
      </script>
</body>

</html>